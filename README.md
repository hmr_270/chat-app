# Chat-App
A basic android chat app


## Libraries and References Used
* CircleImageView Library - https://github.com/hdodenhof/CircleImageView  <br>

* ImageCropper Library - https://github.com/ArthurHub/Android-Image-Cropper <br>

* Picasso Library(For retrieving images from database and displaying) - http://square.github.io/picasso/ <br>

* https://stackoverflow.com/questions/12116092/android-random-string-generator <br>
