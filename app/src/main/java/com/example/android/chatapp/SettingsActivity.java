package com.example.android.chatapp;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private FirebaseUser mCurrentUser;

    private CircleImageView mDisplayImage;
    private TextView mName;
    private TextView mStatus;

    private Button mStatusBtn;
    private Button mImageBtn;

    private String selectedImagePath;

    private static final int GALLARY_PICK = 1;

    //storage reference
    private com.google.firebase.storage.StorageReference mImageStorage;

    private android.app.ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mDisplayImage = (CircleImageView) findViewById(R.id.settings_img);
        mName = (TextView) findViewById(R.id.settings_display_name);
        mStatus = (TextView) findViewById(R.id.settings_status);

        mStatusBtn = (Button) findViewById(R.id.settings_status_btn);
        mImageBtn = (Button) findViewById(R.id.settings_img_btn);

        mImageStorage = com.google.firebase.storage.FirebaseStorage.getInstance().getReference();

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        String current_uid = mCurrentUser.getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String thumb_image = dataSnapshot.child("thumb_image").getValue().toString();

                mName.setText(name);
                mStatus.setText(status);

                com.squareup.picasso.Picasso.with(SettingsActivity.this).load(image).into(mDisplayImage);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String status_value = mStatus.getText().toString();

                Intent status_change = new Intent(SettingsActivity.this, StatusActivity.class);
                status_change.putExtra("status_value", status_value);
                startActivity(status_change);
            }
        });


        mImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*
                com.theartofdev.edmodo.cropper.CropImage.activity()
                        .setGuidelines(com.theartofdev.edmodo.cropper.CropImageView.Guidelines.ON)
                        .start(SettingsActivity.this); */

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLARY_PICK);


            }
        });
    }

        //For image cropping
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if(requestCode == GALLARY_PICK && resultCode == RESULT_OK) {

                android.net.Uri imageuri = data.getData();

                // start cropping activity for pre-acquired image saved on the device
                com.theartofdev.edmodo.cropper.CropImage.activity(imageuri)
                        .setAspectRatio(1,1)
                        .start(this);

                //android.widget.Toast.makeText(SettingsActivity.this, imageuri, android.widget.Toast.LENGTH_LONG).show();

            }

            if (requestCode == com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

                com.theartofdev.edmodo.cropper.CropImage.ActivityResult result = com.theartofdev.edmodo.cropper.CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK) {

                    mProgressDialog = new android.app.ProgressDialog(SettingsActivity.this);
                    mProgressDialog.setTitle("Uploading image");
                    mProgressDialog.setMessage("Please,wait while we upload and process the image.");
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.show();


                    android.net.Uri resultUri = result.getUri();

                    String current_user_id = mCurrentUser.getUid();

                    com.google.firebase.storage.StorageReference Filepath = mImageStorage.child("profile_images").child(current_user_id + ".jpg");

                    Filepath.putFile(resultUri).addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<com.google.firebase.storage.UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@android.support.annotation.NonNull com.google.android.gms.tasks.Task<com.google.firebase.storage.UploadTask.TaskSnapshot> task) {

                            if(task.isSuccessful()){

                                String download_url = task.getResult().getDownloadUrl().toString();

                                mDatabase.child("image").setValue(download_url).addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@android.support.annotation.NonNull com.google.android.gms.tasks.Task<Void> task) {

                                        if(task.isSuccessful()){

                                            mProgressDialog.dismiss();
                                            android.widget.Toast.makeText(SettingsActivity.this, "Success uploading", android.widget.Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });


                            }else{
                                android.widget.Toast.makeText(SettingsActivity.this, "error in uploading", android.widget.Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                            }
                        }
                    });

                } else if (resultCode == com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                    Exception error = result.getError();
                }
            }


    }

    public static String random() {
        java.util.Random generator = new java.util.Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(10);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }
}
