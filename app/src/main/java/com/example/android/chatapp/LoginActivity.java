package com.example.android.chatapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    //for showing appbar at the top
    private Toolbar mToolbar;

    private TextInputLayout mEmail;
    private TextInputLayout mPassword;

    private Button mLoginbtn;

    //Firebase Auth
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    //progress dialog box
    //when user clicks on create in registration form we will show the progress by this box
    private ProgressDialog mLoginProress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //for showing appbar at the top
        mToolbar = (Toolbar) findViewById(R.id.login_tool_bar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //progress dialog box
        //when user clicks on create in registration form we will show the progress by this box
        mLoginProress = new ProgressDialog(this);

        //Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        //android fields
        mEmail = (TextInputLayout)findViewById(R.id.login_email);
        mPassword = (TextInputLayout) findViewById(R.id.login_password);

        mLoginbtn = (Button) findViewById(R.id.login_btn);

        mLoginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEmail.getEditText().getText().toString();
                String password = mPassword.getEditText().getText().toString();

                if(!TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){

                    //progrss dialog box
                    mLoginProress.setTitle("Logging in");
                    mLoginProress.setMessage("Please,wait while we check your credentials.");
                    mLoginProress.setCanceledOnTouchOutside(false);
                    mLoginProress.show();

                    loginuser(email, password);
                }
            }
        });
    }

    private void loginuser(String email, String password){

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){

                    mLoginProress.dismiss();

                    Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    //When user is logged in and press on back button in android it will take the user to register page.
                    //so to avoid that and when user press the back button we will take the user out of app
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                    finish();
                }
                else{

                    mLoginProress.hide();
                    Toast.makeText(LoginActivity.this , "Cannot sign in.Please check the form and try again..", Toast.LENGTH_LONG).show();

                }

            }
        });

    }
}
